"""
Author: Abduarraheem Elfandi
CIS 322
Project 2 - Page server using flask.
"""
from flask import Flask, render_template, abort, request
import os

app = Flask(__name__)

template = "./templates/"

@app.route("/")
def hello():
    return "UOCIS docker demo!"

@app.route("/<path:url>")
def page(url):
    uri = request.environ['REQUEST_URI'] # need to deal with the case where we have // at the beginning of the url (e.g http://localhost:5000//index.html should return 403)
    if url.endswith(".html"):
        if "//" in uri or ".." in uri or "~" in uri:
            abort(403)
        elif not os.path.exists(template+url):
            abort(404)
        else:
            return render_template(url)
    else:
        return abort(404)

@app.errorhandler(404)
def not_found_error(e):
    return render_template("404.html"), 404

@app.errorhandler(403)
def forbidden_error(e):
    return render_template("403.html"), 403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
