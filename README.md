A "getting started" manual for Dockers. CIS 322, Introduction to Software Engineering, at the University of Oregon. Reference: https://docs.docker.com/engine/reference/builder/

## Author: Abduarraheem Elfandi  
## Email: aelfandi@uoregon.edu  
  
  
# Getting started


* Build the simple flask app image using

```
docker build -t uocis-flask-demo .
```
  
* Run the container using
  
```
docker run -d -p 5000:5000 uocis-flask-demo
```

* Launch http://127.0.0.1:5000 using web browser and check the output "UOCIS docker demo!"

# Tasks
  
* The goal of this project is to implement the same "file checking" logic that you implemented in project 1 using flask. 

* Like project 1, if a file ("name.html") exists, transmit "200/OK" header followed by that file html. If the file doesn't exist, transmit an error code in the header along with the appropriate page html in the body. You'll do this by creating error handlers taught in class (refer to the slides; it's got all the tricks needed). You'll also create the following two html files with the error messages. 
    * `404.html` will display "File not found!"
    * `403.html` will display "File is forbidden!"

  Do note that `http://localhost:5000//name.html` will return a 403 error similar to project 1. 
  
# Other
There is a test script `test.sh` (which is based on the script from project 1) to run some test cases for this project.